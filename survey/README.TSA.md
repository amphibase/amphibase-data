# NCBI TSA data

https://www.ncbi.nlm.nih.gov/genbank/tsa/

Transcriptome Sequence Archive (TSA) is an archive of computationally assembled transcript sequences. 

## Current status of TSA records about amphibian species. 

* Download the list of currently available TSA records for amphibia at https://www.ncbi.nlm.nih.gov/Traces/wgs/?page=1&view=tsa&search=amphibia
  * Current version (March, 08, 2020; 62 species): [NCBI_TSA.62_species.2020_03_08.tsv](NCBI_TSA.62_species.2020_03_08.tsv)

## Download all TSA sequences.
* [download-NCBI_TSA.py](./download-NCBI_TSA.py)
```bash
$ /path/to/amphibase/survey/download-NCBI_TSA.py <tsv for TSA records>
```
