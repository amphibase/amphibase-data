#!/bin/bash

# Download BioProject files

TODAY=$(date '+%Y-%m-%d')
echo $TODAY

URL_BP="https://ftp.ncbi.nlm.nih.gov/bioproject/summary.txt"
OUT_BP="BioProject_summary."$TODAY".txt"

wget -O $OUT_BP $URL_BP

# URL_BP="https://ftp.ncbi.nlm.nih.gov/bioproject/bioproject.xml"
# OUT_BP="BioProject_full."$TODAY".xml"
# wget -O $OUT_BP $URL_BP

URL_BS="https://ftp.ncbi.nlm.nih.gov/biosample/biosample_set.xml.gz"
OUT_BS="BioSample_set."$TODAY".xml.gz"

wget -O $OUT_BS $URL_BS

# Download Taxonomy DB dump file

URL_TAX="https://ftp.ncbi.nih.gov/pub/taxonomy/taxdmp.zip"
OUT_TAX="TaxDB_dump."$TODAY".zip"

wget -O $OUT_TAX $URL_TAX
