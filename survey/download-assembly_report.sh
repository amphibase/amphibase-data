#!/bin/bash

# Download BioProject files

TODAY=$(date '+%Y-%m-%d')
echo $TODAY

URL_REFSEQ="https://ftp.ncbi.nlm.nih.gov/genomes/ASSEMBLY_REPORTS/assembly_summary_refseq.txt"
OUT_REFSEQ="assembly_summary_refseq."$TODAY".txt"

wget -O $OUT_REFSEQ $URL_REFSEQ
pigz -p 4 $OUT_REFSEQ

URL_GB="https://ftp.ncbi.nlm.nih.gov/genomes/ASSEMBLY_REPORTS/assembly_summary_genbank.txt"
OUT_GB="assembly_summary_genbank."$TODAY".txt"

wget -O $OUT_GB $URL_GB
pigz -p 4 $OUT_GB
