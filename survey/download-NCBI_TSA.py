#!/usr/bin/env python3
import sys
import urllib.request

# download csv file from the following link:
# https://www.ncbi.nlm.nih.gov/Traces/wgs/?page=1&view=tsa&search=amphibia

filename_tsv = sys.argv[1]

url_base = 'http://ftp.ncbi.nlm.nih.gov/genbank/tsa'

f_tsv = open(filename_tsv, 'r')
h_tsv = f_tsv.readline().strip().split(",")
for line in f_tsv:
    tokens = line.strip().split(",")
    prefix_s = tokens[h_tsv.index('prefix_s')]
    species = tokens[h_tsv.index('organism_an')].replace(' ', '_')
    filename_out = '%s.tsa.%s.1.fsa_nt.gz' % (species, prefix_s)

    url_fa = '%s/%s/tsa.%s.1.fsa_nt.gz' % (url_base, prefix_s[0], prefix_s[:4])
    sys.stderr.write('Write %s\n from %s\n' % (filename_out, url_fa))

    f_out = open(filename_out, 'wb')
    with urllib.request.urlopen(url_fa) as tsa_fasta:
        f_out.write(tsa_fasta.read())
    f_out.close()
f_tsv.close()
